// VALIDAMOS QUE LA MATRICULA TENGA INFORMACION
// CUANDO DE CLICK AL BOTON DE BUSCAR

import alumnosDb from "../../models/alumnos";

 const btnBuscar = document.getElementById('btnBuscar');

 // EVENTO CLICK
btnBuscar.addEventListener('click',buscar);

function buscar(){
     const matricula = document.getElementById('matricula').value;
      
     if(!matricula){
        alert("Falto capturar la matricula");
     }else{
        alumnosDb.consultarMatricula(matricula);
     }
}

